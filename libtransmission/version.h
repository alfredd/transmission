#define PEERID_PREFIX             "-TR284Z-"
#define USERAGENT_PREFIX          "2.84+"
#define SVN_REVISION              "14344"
#define SVN_REVISION_NUM          14344
#define SHORT_VERSION_STRING      "2.84+"
#define LONG_VERSION_STRING       "2.84+ (14344)"
#define VERSION_STRING_INFOPLIST  2.84+
#define MAJOR_VERSION             2
#define MINOR_VERSION             84
#define TR_NIGHTLY_RELEASE        1
